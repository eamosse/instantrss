# README #

An Android application that allows users to consume public feed from media news such as Lemonde, Le Point or France Info. 
First, users can select feed from a list of predefined feeds, then they can visualise the latest news pubblished in the selected feed. 
In addition, users can add article as favorite, save to read later or share them with their friends using social media applications. 

## Available Features ##
In this section, I describe the functionnal features of the application. 

### Add feed ###
This feature allows the end user to choose the feed he wants to consume from a list of public feeds embedded in the application. 
The available feeds include: 

* http://www.lemonde.fr/rss/une.xml
* http://www.lepoint.fr/24h-infos/rss.xml
* https://www.franceinter.fr/rss/a-la-une.xml
* http://www.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/
* https://www.francetvinfo.fr/titres.rss

For each feed selected by the user, an entry is added in the main menu so that the end user can easily view the latest articles of the feed. 

### View latest articles in a feed ###
This feature allows a user to visualise the latest articles published in the selected feeds. Each article is displayed as a card containing the following infos :
* the title of the article 
* a short description 
* an image 
* the date 
* and the icon of the the publisher 

When the user clicks on a card, the full content of the article is displayed in a Webview. 
In addition, each card contains a set of actions that allows the user to share, add as favorite or save to read later. 

### View Favorites ###
Using this feature, the end user is able to visualise his favorites articles. 
The list is displayed in a similar way as the latest articles. Also the menu embedded in the card allows the user to remove an article from his favorites. 

### View Articles to read ###
This feature aims at allowing the user to add articles in a toread list. An entry in the main menu allows the user to visualize these articles. 

### Read article ###
When the user clicks on a card, the full content of the article is displayed in a Webview. 
From that view, the user is also able to add an article as favorite, in his toread list or share it with friends thanks the the actions available in the action bar.  

## Technical implementations ##
In this section I describe the technical implementation of the application. 
I start by describing the architecture of the application, then I describe the implementation of the main modules such as the feed parser or the data model.  

### Architecture of the application ###
The application is built as a native Android application following the default architecture of an Android application. Java classes are organized in packages and static ressources are grouped in folders. In general, classes that contain ojects of the same categories are grouped in the same packages; for instance the package named activities contains all the activities of the application and the package named models contains the business model classes. 

### Technical Considerations ###
In this section, I focalize on the main technical concepts used in the application. 

* Loading data from internet 

Since the application is fully based on data provide by external sources, we use the HTTP protocol to download the content. For that, we use OkHttp [1], a well-known library to perform the http request. 
In order to avoid blocking the UI thread while performing the request, we use the Bolt Library [2] to execute the requests as jobs and also to ease the communication between backgrounds and the ui threads. The rational behind using Bold instead combining Java Threads and interfaces for callback is that Bolt makes it easy to chaining jobs and keeps the code clearer and easier to maintain. 

The following snippet shows in example of the request to download the articles for a feed identified by its url: 

````
Task.callInBackground(new Callable<List<Article>>() {
        @Override
        public List<Article> call() throws Exception {
            List<Article> articles = new FeedParser().fetchData(selectedChannel.getLink());
            ...
            return articles;
        }
    }).continueWith(new Continuation<List<Article>, Void>() {
        @Override
        public Void then(Task<List<Article>> task) throws Exception {
            progressBar.setVisibility(View.GONE);
            if (task.isFaulted()) {
                setTextMessage(task.getError().getMessage());
            } else {
                bindArticles(task.getResult());
            }
            return null;
        }
    }, Task.UI_THREAD_EXECUTOR);
````
In this example, we first create a task to download and parse the feed in a background task; then when the background task is complete, we create a new task in the main thread to update the UI (in that case display the list of articles in a recycler view).

* The feed parser

The most important module in the application is to convert the feeds into POJO. To perform this task, we use xmlpull [3], an XML parser. The library starts by building a tree from the XML document and provides simple method to browse the leafs of the tree and retrive the data. It is less powerful than other libraries such as XMLDom. In the specific case of rss feed where the documents contains most of the time tags and text, I found this library suitable for this task. Moreover, since it is already included in the Android SDK and doest not require third party library. 

In my implementation, I assume that the feed follows a common format. More specifically, I consider that the root documment is 'rss' and encapsulates a channel tag. The channel contains information related to the publisher such as title, description or pulication date and a list of items (i.e the articles). Each item contains a list of tag such as title, description, link or pubdate. With that in mind, I browsed the xml tree to extract information about a publisher and its latest articles. 

* Persistance

In other important feature in the application is the usage of a local database to manage user favorites, toread list or the selected feeds. It is also used to save last articles of a feed so that the user can still visualise them even offline. We use the Room Framework [4] as an ORM to persist objects (article, channel) in the local database and to manage relationships between the entities. For instance, article and channel are linked by a unidirectional one-to-many relationships, thus when a channel is removed, all its articles are also removed.  

### Conclusions et Perspectives ###
In this project, I developed an rss parser that allows a user to visualise the latest articles published by media news. The application allows a user to select the feed he wishes to visualise and present the latest articles of the feed. Also, the end user is able to add article as favorite, in a toread list or share them via social networks application. 

#### Possible improvement ####
The application is at its early stage, thus there are many rooms for improvements such as: 

* Notify the end user whenever new articles are published, this can be easily done by using a service that monitors the feed according to a predifined timer (e.g. every 1 hour) and if the modification date of the feed is different to the one in the local database, one may conclude that new articles are available. 

* Add publisher as favorite; in the current implementation, I select the first available feed by default, it would be interesting to let the end user choose his favorites. 

* Improve the parser : The current parser highly depends on the format of the feed, it would be interesting to automatically detect the format and adapt the parser accordingly. 

Besides fonctionnal improvement, I should also: 

* Create unit test for several classes and methods such as : the parser or the data layers
* Create automatic UI test to avoid regression  
* Handle configuration changes such as: screen orientation 

### References ###

[1] OKHttp : http://square.github.io/okhttp/

[2] Android Bolt : https://github.com/BoltsFramework/Bolts-Android 

[3] XMLPull: https://developer.android.com/reference/org/xmlpull/v1/package-summary.html

[4 Android Room: https://developer.android.com/topic/libraries/architecture/room.html