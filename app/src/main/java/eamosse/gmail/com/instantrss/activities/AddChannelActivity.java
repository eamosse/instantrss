package eamosse.gmail.com.instantrss.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.adapters.ChannelAdapter;
import eamosse.gmail.com.instantrss.dal.DataStore;
import eamosse.gmail.com.instantrss.handlers.ChannelHandler;
import eamosse.gmail.com.instantrss.models.Channel;


/**
 * Created by eamosse on 23/01/2018.
 */

public class AddChannelActivity extends AppCompatActivity implements ChannelHandler{
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_add_channel);
        init();
    }
    
    void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSupportActionBar(toolbar);
        RecyclerView recyclerView;
        //setuo the recycler view
        recyclerView = findViewById(R.id.suggested_channel);
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        ChannelAdapter adapter = new ChannelAdapter(getResources().getStringArray(R.array.channels), this);
        recyclerView.setAdapter(adapter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onAdd(final Channel channel) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DataStore.getAppDatabase(context).articleDAO().insert(channel);
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task<Void> task) throws Exception {
                Intent intent = new Intent("feed_added");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                Toast.makeText(context, R.string.channel_added_message, Toast.LENGTH_LONG).show();
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }
}
