package eamosse.gmail.com.instantrss.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.adapters.DrawerAdapter;
import eamosse.gmail.com.instantrss.fragments.BaseFragment;
import eamosse.gmail.com.instantrss.dal.DataStore;
import eamosse.gmail.com.instantrss.fragments.ArticleListFragment;
import eamosse.gmail.com.instantrss.models.Channel;
import eamosse.gmail.com.instantrss.models.NavigationItem;
import eamosse.gmail.com.instantrss.models.NavigationType;

/**
 * The main class of the application
 * It manages the whole lyfecycle of the app
 */
public class HomeActivity extends BaseActivity {

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ListView drawerList;
    ArrayList<NavigationItem> navigationItems;

    private ActionBarDrawerToggle drawerToggle;

    // use a local broadcast to monitor changes in the registred feeds and adapt the drawer accordingly
    private BroadcastReceiver mFeedWatcher = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //we simply rebind the menu
            setupNavigationItems();
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList = findViewById(R.id.main_navigation);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.nav_header_home, drawerList, false);
        drawerList.addHeaderView(header, null, false);

        //handle item click in the drawer actions
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NavigationItem item = navigationItems.get((int)id);
                if (item.getType() == NavigationType.ADD) {
                    showAddFeed();
                } else {
                    setList(item.channel, item.getType());
                }
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        setupNavigationItems();
        setupDrawerAndToggle();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //unregister
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mFeedWatcher);

        setDefaultContent();
    }

    /**
     * bind menu items in the navigation drawer
     */
    private void setupNavigationItems() {
        navigationItems = new ArrayList<>();

        Task.callInBackground(new Callable<List<Channel>>() {
            @Override
            public List<Channel> call() throws Exception {
                //load the registered channels in the local db
                return DataStore.getAppDatabase(getBaseContext()).articleDAO().allChannel();
            }
        }).continueWith(new Continuation<List<Channel>, Void>() {
            @Override
            public Void then(Task<List<Channel>> task) throws Exception {
                //new group for the feeds
                navigationItems.add(new NavigationItem(0, getString(R.string.latest_news), true, NavigationType.NONE));

                //for each feed availale, we add an entry in the navigation drawer
                for (Channel aChannel: task.getResult()) {
                    NavigationItem navigationItem = new NavigationItem(R.drawable.ic_read_later, aChannel.getTitle(), false, NavigationType.LATEST);
                    navigationItem.channel = aChannel;
                    navigationItems.add(navigationItem);
                }

                //new group for user personal actions
                navigationItems.add(new NavigationItem(0, getString(R.string.menu_profile), true, NavigationType.NONE));

                navigationItems.add(new NavigationItem(R.drawable.ic_add_channel, getString(R.string.add_channel), false, NavigationType.ADD));
                navigationItems.add(new NavigationItem(R.drawable.ic_add_favorite, getString(R.string.my_favorites), false, NavigationType.FAVORITES));
                navigationItems.add(new NavigationItem(R.drawable.ic_read_later, getString(R.string.read_later), false, NavigationType.TOREAD));

                DrawerAdapter mAdapter = new DrawerAdapter(getBaseContext(), navigationItems);
                drawerList.setAdapter(mAdapter);
                drawerLayout.invalidate();


                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);

    }

    /**
     * setup the drawer behavior
     */
    private void setupDrawerAndToggle() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setDrawerIndicatorEnabled(true);
            }
        };
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected ActionBarDrawerToggle getDrawerToggle() {
        return drawerToggle;
    }

    @Override
    protected DrawerLayout getDrawer() {
        return drawerLayout;
    }

    /**
     * show the list fragment according to a given type
     * @param channel - The selected feed
     * @param type - the associated type in the drawer menu
     */
    private void setList(@Nullable  Channel channel, @NonNull NavigationType type) {
        Fragment currentFragment = getCurrentFragment(getSupportFragmentManager());
        if (currentFragment != null && currentFragment.getClass() == ArticleListFragment.class) {
            ((ArticleListFragment)currentFragment).onNewType(channel, type);
        } else {
            ArticleListFragment fragment = ArticleListFragment.newInstance(channel, type);
            //bind the fragment in the activity and manage the stack
            add(fragment);
        }
    }

    /**
     *
     * @param fragmentManager the fragment manager
     * @return the top base fragment
     */
    @Nullable
    public BaseFragment getCurrentFragment(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            return null;
        }
        FragmentManager.BackStackEntry currentEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);

        String tag = currentEntry.getName();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        return (BaseFragment) fragment;
    }

    void showAddFeed() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mFeedWatcher, new IntentFilter("feed_added"));
        Intent intent = new Intent(getBaseContext(), AddChannelActivity.class);
        startActivity(intent);
    }

    /**
     * Initialise the article list, when lauching the application
     */
    void setDefaultContent() {
        Task.callInBackground(new Callable<Channel>() {
            @Override
            public Channel call() throws Exception {
                //first load the registered feeds
                List<Channel> channels = DataStore.getAppDatabase(getBaseContext()).articleDAO().allChannel();
                //if no feed is available, this is a fresh version of the app
                if (!channels.isEmpty()) {
                    //TODO: set a default channel instead
                    return channels.get(0);
                }
                return null;
            }
        }).continueWith(new Continuation<Channel, Void>() {
            @Override
            public Void then(Task<Channel> task) throws Exception {
                //if a feed is returned, then load the latest articles
                if (task.getResult() != null) {
                    setList(task.getResult(), NavigationType.LATEST);
                } else {
                    showAddFeed();
                }
                //if not, ask the user to initialize the app for a first usage
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

}
