package eamosse.gmail.com.instantrss.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.handlers.ArticleSelectedCallback;
import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.views.ArticleViewHolder;

/**
 * Created by eamosse on 17/01/2018.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder>{
    List<Article> articles;
    ArticleSelectedCallback callback;

    public ArticleAdapter(List<Article> articles, ArticleSelectedCallback callback) {
        this.articles = articles;
        this.callback = callback;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_news, parent, false);
        return new ArticleViewHolder(v);
    }

    public void removeItem(Article article) {
        this.articles.remove(article);
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        final Article article = articles.get(position);
        holder.bind(article);
        holder.getCv().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelected(article);
            }
        });

        holder.getArticleOptions().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onAction(article, view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

}