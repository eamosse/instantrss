package eamosse.gmail.com.instantrss.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.handlers.ChannelHandler;
import eamosse.gmail.com.instantrss.views.ChannelViewHolder;

/**
 * Created by eamosse on 23/01/2018.
 */

public class ChannelAdapter extends RecyclerView.Adapter<ChannelViewHolder> {
    String[] selectedFlux;
    ChannelHandler callback;

    public ChannelAdapter(String[] selectedFlux, ChannelHandler callback) {
        this.callback = callback;
        this.selectedFlux = selectedFlux;
    }

    @Override
    public ChannelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_channel, parent, false);
        return new ChannelViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ChannelViewHolder holder, int position) {
        final String url = selectedFlux[position];
        holder.bind(url);
        holder.getAddButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onAdd(holder.getChannel());
            }
        });
    }

    @Override
    public int getItemCount() {
        return selectedFlux.length;
    }

}
