package eamosse.gmail.com.instantrss.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.models.NavigationItem;

/**
 * Created by eamosse on 22/01/2018.
 */

public class DrawerAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private List<NavigationItem> items;
    Context context;

    public DrawerAdapter(Context context, List<NavigationItem> items) {
        this.items = items;
        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public NavigationItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        NavigationItem item = getItem(position);
        return item.isHeader() ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NavigationViewHolder viewHolder;

        if(convertView == null){
            viewHolder =  new NavigationViewHolder();
            if (getItemViewType(position) == TYPE_ITEM) {
                convertView = mInflater.inflate(R.layout.navigation_item,parent, false);
                viewHolder.titleView = convertView.findViewById(R.id.navigation_title);
                viewHolder.imageView = convertView.findViewById(R.id.navigation_icon);
            } else {
                convertView = mInflater.inflate(R.layout.navigation_item_group,parent, false);
                viewHolder.titleView = convertView.findViewById(R.id.navigation_header);
            }

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NavigationViewHolder) convertView.getTag();
        }

        viewHolder.bind(items.get(position));

        return convertView;
    }

    class NavigationViewHolder{
        TextView titleView;
        ImageView imageView;

        void bind(NavigationItem item) {
            titleView.setText(item.title);
            if (!item.isHeader()) {
                imageView.setImageResource(item.icon);
            }


        }
    }
}
