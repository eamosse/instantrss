package eamosse.gmail.com.instantrss.dal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.models.Channel;

/**
 * Created by eamosse on 17/01/2018.
 */

@Dao
public interface ArticleDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Article> instances);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(Article... instance);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(Channel instance);

    @Delete
    void delete(List<Article> instances);

    @Delete
    void delete(Article instance);

    @Query("SELECT * FROM article WHERE isFavorite = 1;")
    List<Article> findFavorites();

    @Query("SELECT * FROM article WHERE isDelayed = 1;")
    List<Article> findToRead();

    @Query("SELECT * FROM article WHERE feedId = :feed ORDER BY publicationDate ASC")
    List<Article> findAll(String feed);

    @Query("Select * from channel where link = :link")
    Channel getChannel(String link);

    @Query("Select * from channel")
    List<Channel> allChannel();

    @Query("DELETE FROM article WHERE isDelayed = 0 AND isFavorite = 0;")
    void deleteArticles();
}
