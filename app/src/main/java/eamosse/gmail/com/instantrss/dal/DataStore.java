package eamosse.gmail.com.instantrss.dal;

/**
 * Created by eamosse on 03/11/2017.
 */

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.models.Channel;

@Database(entities = {
        Channel.class,
        Article.class},
          version = 1)
public abstract class DataStore extends RoomDatabase {

    private static DataStore instance;

    public abstract ArticleDAO articleDAO();

    public static DataStore getAppDatabase(Context context) {
        if (instance == null) {
            instance =
                    Room.databaseBuilder(context.getApplicationContext(), DataStore.class, "feed-database")
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }


    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

}

