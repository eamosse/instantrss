package eamosse.gmail.com.instantrss.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.handlers.ListSupportFragment;
import eamosse.gmail.com.instantrss.helpers.Constants;
import eamosse.gmail.com.instantrss.models.Article;

/**
 * Created by eamosse on 20/01/2018.
 */

//TODO: manage screen orientation
public class ArticleDetailFragment extends BaseFragment {
    private Article article;
    private ListSupportFragment supportFragment;

    public static ArticleDetailFragment newInstance(Article article, ListSupportFragment listSupportFragment) {
        Bundle args = new Bundle();
        ArticleDetailFragment fragment = new ArticleDetailFragment();
        fragment.setArguments(args);
        fragment.article = article;
        fragment.supportFragment = listSupportFragment;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected String getTitle() {
        return article != null ? article.getTitle() : getString(R.string.app_name);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(Constants.ARTICLE_KEY)) {
            article = savedInstanceState.getParcelable(Constants.ARTICLE_KEY);
        }
        View contentView = inflater.inflate(R.layout.activity_article_view, container, false);
        WebView mWebView;

        final ProgressBar progressBar = contentView.findViewById(R.id.progressBar);
        mWebView = contentView.findViewById(R.id.article_webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }
        });
        mWebView.loadUrl(article.getLink());
        return contentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.article_options, menu);

        //set favorite icone
        MenuItem favoriteItem = menu.findItem(R.id.article_favorite);
        setFavorateIcon(article, favoriteItem);

        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            drawable.mutate();
            drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean selected = handleMenu(article, item);
        supportFragment.onReload();
        return selected;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Constants.ARTICLE_KEY, article);
    }
}
