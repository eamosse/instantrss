package eamosse.gmail.com.instantrss.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.adapters.ArticleAdapter;
import eamosse.gmail.com.instantrss.dal.DataStore;
import eamosse.gmail.com.instantrss.handlers.ArticleSelectedCallback;
import eamosse.gmail.com.instantrss.handlers.ListSupportFragment;
import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.models.Channel;
import eamosse.gmail.com.instantrss.models.NavigationType;
import eamosse.gmail.com.instantrss.parser.FeedParser;

/**
 * Created by eamosse on 17/01/2018.
 */

//TODO: manage screen orientation
public class ArticleListFragment extends BaseFragment implements ArticleSelectedCallback, ListSupportFragment {
    RecyclerView recyclerView;
    TextView emptyTextView;
    Context context;
    NavigationType type = NavigationType.LATEST;
    ProgressBar progressBar;
    ArticleAdapter adapter;
    Channel selectedChannel;

    public static ArticleListFragment newInstance(Channel channel, NavigationType type) {

        Bundle args = new Bundle();

        ArticleListFragment fragment = new ArticleListFragment();
        fragment.setArguments(args);
        fragment.selectedChannel = channel;
        fragment.type = type;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    protected String getTitle() {
        return getString(R.string.latest_news);
    }

    void setTitle() {

        String title;

        switch (type) {
            case FAVORITES:
                title = getString(R.string.fragment_favorite);
                break;

            case TOREAD:
                title = getString(R.string.fragment_toread);
                break;

            default:
                if (selectedChannel != null) {
                    title = getString(R.string.fragment_latest, selectedChannel.getTitle());
                } else {
                    title = getString(R.string.fragment_latest, "");
                }
                break;
        }
        getActivity().setTitle(title);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    public void onNewType(Channel channel, NavigationType type) {
        this.type = type;
        this.selectedChannel = channel;
        loadArticles(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.activity_list, container, false);
        emptyTextView = contentView.findViewById(R.id.empty);
        progressBar = contentView.findViewById(R.id.progressBar);
        //setuo the recycler view
        recyclerView = contentView.findViewById(R.id.article_list);
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        //load the articles
        loadArticles(true);

        return contentView;
    }

    private void bindArticles(List<Article> articles) {
        if (articles.isEmpty()) {
            setEmptyMessage();
            return;
        }
        emptyTextView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter = new ArticleAdapter(articles, ArticleListFragment.this);
        recyclerView.setAdapter(adapter);
    }

    private void setEmptyMessage() {
        int message;
        switch (type) {

            case FAVORITES:
                message = R.string.no_favorites;
                break;

            case TOREAD:
                message = R.string.no_delayed;
                break;

            default:
                message = R.string.no_articles;
                break;
        }
        setTextMessage(getString(message));

    }

    private void setTextMessage(String message) {
        recyclerView.setVisibility(View.GONE);
        emptyTextView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        emptyTextView.setText(message);
    }

    /**
     * Load the articles according to the given type
     */
    private void loadArticles(final boolean refresh) {
        setTitle();
        if (type == NavigationType.LATEST && selectedChannel == null) {
            setTextMessage(getString(R.string.no_channel_selected));
            return;
        }

        Task.callInBackground(new Callable<List<Article>>() {
            @Override
            public List<Article> call() throws Exception {

                switch (type) {

                    case FAVORITES:
                        return DataStore.getAppDatabase(context).articleDAO().findFavorites();

                    case TOREAD:
                        return DataStore.getAppDatabase(context).articleDAO().findToRead();

                    default:
                        return DataStore.getAppDatabase(context).articleDAO().findAll(selectedChannel.getLink());

                }
            }
        }).continueWith(new Continuation<List<Article>, Void>() {
            @Override
            public Void then(Task<List<Article>> task) throws Exception {
                if (type == NavigationType.LATEST && refresh) {
                    reloadFeed();
                }
                bindArticles(task.getResult());
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    private void reloadFeed() {
        progressBar.setVisibility(View.VISIBLE);

        Task.callInBackground(new Callable<List<Article>>() {
            @Override
            public List<Article> call() throws Exception {
                List<Article> articles = new FeedParser().fetchArticles(selectedChannel.getLink());
                List<Article> favorites = DataStore.getAppDatabase(context).articleDAO().findFavorites();
                List<Article> toread = DataStore.getAppDatabase(context).articleDAO().findToRead();

                for (Article article : articles) {
                    article.setFavorite(favorites.contains(article));
                    article.setDelayed(toread.contains(article));
                    article.setFeedId(selectedChannel.getLink());
                }
                DataStore.getAppDatabase(getContext()).articleDAO().insertAll(articles);
                return articles;
            }
        }).continueWith(new Continuation<List<Article>, Void>() {
            @Override
            public Void then(Task<List<Article>> task) throws Exception {
                progressBar.setVisibility(View.GONE);
                if (task.isFaulted()) {
                    setTextMessage(task.getError().getMessage());
                } else {
                    bindArticles(task.getResult());
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    @Override
    public void onSelected(Article article) {
        Fragment detailFragment = ArticleDetailFragment.newInstance(article, this);
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.content_frame, detailFragment, "detail")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("home")
                .commit();
    }

    @Override
    @SuppressLint("RestrictedApi")
    public void onAction(final Article article, View view) {

        PopupMenu popMenu = new PopupMenu(getContext(), view);
        popMenu.inflate(R.menu.article_options);
        popMenu.show();

        //set favorite icone
        MenuItem favoriteItem = popMenu.getMenu().findItem(R.id.article_favorite);
        setFavorateIcon (article, favoriteItem);

        popMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean selected = handleMenu(article, item);
                if(type == NavigationType.FAVORITES && !article.isFavorite()) {
                    adapter.removeItem(article);
                }
                return selected;
            }
        });

        popMenu.show();
    }

    @Override
    public void onReload() {
        //just reload the articles from the dbb
        //TODO: maintain list state while reloading
        loadArticles(false);
    }
}
