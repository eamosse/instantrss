package eamosse.gmail.com.instantrss.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.view.MenuItem;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.dal.DataStore;
import eamosse.gmail.com.instantrss.handlers.AddFragmentHandler;
import eamosse.gmail.com.instantrss.models.Article;

public abstract class BaseFragment extends Fragment {

    private AddFragmentHandler fragmentHandler;

    protected void update() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        fragmentHandler = new AddFragmentHandler(getActivity().getSupportFragmentManager());
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getTitle());
    }

    protected abstract String getTitle();

    protected void add(BaseFragment fragment) {
        fragmentHandler.add(fragment);
    }

    protected void saveArticle(final Article article) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataStore.getAppDatabase(getContext()).articleDAO().insert(article);
            }
        }).start();
    }


    protected void setFavorateIcon(Article article, MenuItem item) {
        item.setTitle(article.isFavorite() ? R.string.remove_favorite : R.string.add_favorite);
        item.setIcon(article.isFavorite() ? R.drawable.ic_remove_favorite : R.drawable.ic_add_favorite);
    }

    protected boolean handleMenu(Article article, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.article_share:
                ShareCompat.IntentBuilder.from(getActivity())
                        .setType("text/plain")
                        .setChooserTitle(R.string.article_share)
                        .setText(article.getLink())
                        .startChooser();
                break;

            case R.id.article_favorite:
                article.setFavorite(!article.isFavorite());
                saveArticle(article);
                setFavorateIcon(article, item);
                break;

            case R.id.article_delayed:
                article.setDelayed(!article.isDelayed());
                saveArticle(article);
                break;

            default:

                break;
        }
        return false;
    }
}
