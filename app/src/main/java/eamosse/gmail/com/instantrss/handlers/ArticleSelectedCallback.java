package eamosse.gmail.com.instantrss.handlers;

import android.view.View;

import eamosse.gmail.com.instantrss.models.Article;

/**
 * Created by eamosse on 18/01/2018.
 */

public interface ArticleSelectedCallback {
    void onSelected(Article article);
    void onAction(Article article, View view);
}
