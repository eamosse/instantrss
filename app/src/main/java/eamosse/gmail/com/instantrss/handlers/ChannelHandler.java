package eamosse.gmail.com.instantrss.handlers;

import eamosse.gmail.com.instantrss.models.Channel;

/**
 * Created by eamosse on 23/01/2018.
 */

public interface ChannelHandler {
    void onAdd(Channel channel);
}
