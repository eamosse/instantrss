package eamosse.gmail.com.instantrss.helpers;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Map date to timestamp and vice versa
 * Created by edouard on 18/01/2018.
 */
public class DateConverter {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
