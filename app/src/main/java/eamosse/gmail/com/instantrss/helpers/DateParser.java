package eamosse.gmail.com.instantrss.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Utils methods for date objects
 * Created by edouard on 19/01/2018.
 */

public class DateParser {
    public static final String MONDE_DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
    public static final String DAYS_KEY = "days";
    public static final String HOURS_KEY = "hours";
    public static final String MINUTES_KEY = "minutes";
    public static final String SECONDS_KEY = "seconds";

    /**
     * Convert a string into date regarding the given format
     * @param date
     * @param format
     * @return a date object or null if the conversion fails
     */
    public static Date toDate(String date, String format) {
        DateFormat df = new SimpleDateFormat(format, Locale.US);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Compute the duration between a given date and the current date
     * @param date
     * @return a hashmap containing the duration for days, hours, minutes and seconds
     */
    public static Map<String, Integer> duration(Date date) {
        //get the current date
        Date now = new Date();
        //get the difference in millis between the given date and now
        long difference = Math.abs(now.getTime() - date.getTime());

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = difference / daysInMilli;
        difference = difference % daysInMilli;

        long elapsedHours = difference / hoursInMilli;
        difference = difference % hoursInMilli;

        long elapsedMinutes = difference / minutesInMilli;
        difference = difference % minutesInMilli;

        long elapsedSeconds = difference / secondsInMilli;

        Map<String, Integer> result = new HashMap<>();
        result.put(DAYS_KEY, (int) elapsedDays);
        result.put(HOURS_KEY, (int) elapsedHours);
        result.put(MINUTES_KEY, (int) elapsedMinutes);
        result.put(SECONDS_KEY, (int) elapsedSeconds);

        return result;
    }
}
