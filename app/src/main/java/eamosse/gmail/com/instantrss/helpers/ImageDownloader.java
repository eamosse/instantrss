package eamosse.gmail.com.instantrss.helpers;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import eamosse.gmail.com.instantrss.R;

/**
 * Download images using picasso
 * Created by eamosse on 17/01/2018.
 */

public class ImageDownloader {

    /**
     * download the image from the URL and load it into the imageview
     *
     * @param url
     * @param imageView
     * @param crop      if yes, the image is crop to fit the imageview size
     */
    public static void setImage(String url, final ImageView imageView, boolean crop) {
        RequestCreator loader = Picasso.with(imageView.getContext())
                .load(url)
                .placeholder(R.mipmap.ic_launcher);

        if (crop) {
            loader.fit();
            loader.centerCrop();
        }

        loader.into(imageView);
    }

}
