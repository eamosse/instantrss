package eamosse.gmail.com.instantrss.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Date;

import eamosse.gmail.com.instantrss.helpers.DateConverter;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Model to manage the article
 * Created by eamosse on 17/01/2018.
 */

@Entity(foreignKeys = @ForeignKey(entity = Channel.class,
        parentColumns = "link",
        childColumns = "feedId",
        onDelete = CASCADE))
public class Article implements Comparable<Article>, Parcelable {

    @PrimaryKey
    private @NonNull String guid;
    private @NonNull String title;
    private @NonNull String description;
    private @NonNull String link;
    private String image;
    private String feedId;

    @TypeConverters({DateConverter.class})
    private @NonNull Date publicationDate;

    private boolean isFavorite = false;
    private boolean isDelayed = false;
    private boolean isRead = false;

    public Article(@NonNull  String guid, @NonNull String title, @NonNull String description, @NonNull String link, @NonNull Date publicationDate) {
        this.guid = guid;
        this.title = title;
        this.description = description;
        this.link = link;
        this.publicationDate = publicationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isDelayed() {
        return isDelayed;
    }

    public void setDelayed(boolean delayed) {
        isDelayed = delayed;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int compareTo(@NonNull Article o) {
        return o.getGuid().compareToIgnoreCase(this.getGuid());
    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.guid);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.link);
        dest.writeString(this.image);
        dest.writeString(this.feedId);
        dest.writeLong(this.publicationDate != new Date() ? this.publicationDate.getTime() : -1);
        dest.writeByte(this.isFavorite ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isDelayed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isRead ? (byte) 1 : (byte) 0);
    }

    protected Article(Parcel in) {
        this.guid = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.link = in.readString();
        this.image = in.readString();
        this.feedId = in.readString();
        long tmpPublicationDate = in.readLong();
        this.publicationDate = tmpPublicationDate == -1 ? new Date() : new Date(tmpPublicationDate);
        this.isFavorite = in.readByte() != 0;
        this.isDelayed = in.readByte() != 0;
        this.isRead = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    @Override
    public String toString() {
        return this.getGuid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        return guid.equals(article.guid);
    }

    @Override
    public int hashCode() {
        return guid.hashCode();
    }
}
