package eamosse.gmail.com.instantrss.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import eamosse.gmail.com.instantrss.helpers.DateConverter;

/**
 * Business model to manage the feed publisher
 * Created by eamosse on 21/01/2018.
 */

@Entity
public class Channel {

    private String title;
    private String description;
    private String image;
    @PrimaryKey
    private @NonNull String link;

    @TypeConverters({DateConverter.class})
    public Date date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
