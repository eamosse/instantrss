package eamosse.gmail.com.instantrss.models;

/**
 * Model for the item menu
 * Created by eamosse on 22/01/2018.
 */

public class NavigationItem {
    public final String title;
    public final int icon;
    private final boolean isHeader;
    public Channel channel;

    private NavigationType type;

    public NavigationItem(int icon, String title, boolean isHeader, NavigationType type) {
        this.title = title;
        this.icon = icon;
        this.isHeader = isHeader;
        this.type = type;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public NavigationType getType() {
        return type;
    }

    public void setType(NavigationType type) {
        this.type = type;
    }

}
