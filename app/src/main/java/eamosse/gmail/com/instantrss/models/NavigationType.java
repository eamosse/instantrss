package eamosse.gmail.com.instantrss.models;

/**
 * Types of the entries in the menu
 * Created by eamosse on 22/01/2018.
 */

public enum NavigationType {
    LATEST,
    FAVORITES,
    TOREAD,
    ADD,
    NONE
}
