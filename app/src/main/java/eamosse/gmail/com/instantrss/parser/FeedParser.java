package eamosse.gmail.com.instantrss.parser;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eamosse.gmail.com.instantrss.helpers.DateParser;
import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.models.Channel;
import eamosse.gmail.com.instantrss.requester.RequestException;
import eamosse.gmail.com.instantrss.requester.Requester;

/**
 * A parser for the rss feeds
 * Created by aedouard on 17/01/18.
 */
public class FeedParser {
    private static final String ITEM_TITLE = "title";
    private static final String ITEM_DESCRIPTION = "description";
    private static final String ITEM_LINK = "link";
    private static final String ITEM_DATE = "pubDate";
    private static final String ITEM_GUID = "guid";
    private static final String ITEM_IMAGE = "image";
    private static final String ITEM_ENCLOSURE = "enclosure";
    private static final String DOC_ROOT = "rss";
    private static final String DOC_ITEM = "item";

    /**
     * Retrive information about the publisher of a feed
     * @param url
     * @return a channel
     * @throws RequestException
     * @throws XmlPullParserException
     * @throws IOException
     */
    public Channel fetchFeed(String url) throws RequestException, XmlPullParserException, IOException {
        InputStream stream = getStream(url);
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(stream, null);
        parser.nextTag();
        Channel channel = readChannel(parser);
        channel.setLink(url);
        return channel;
    }

    /**
     * Given an rss feed, retrieve the properties of the publisher
     * @param parser
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private Channel readChannel(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, null, DOC_ROOT);
        int eventType = parser.getEventType();
        String currentTag;

        Channel channel = new Channel();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            eventType = parser.next();
            if (parser.getName() != null && parser.getName().equalsIgnoreCase(DOC_ITEM)) {
                break;
            }
            if (eventType == XmlPullParser.START_TAG) {
                currentTag = parser.getName();
                switch (currentTag) {

                    case ITEM_TITLE:
                        channel.setTitle(readBasicTag(parser, currentTag));
                        break;

                    case ITEM_DESCRIPTION:
                        channel.setDescription(readBasicTag(parser, currentTag));
                        break;

                    case ITEM_DATE:
                        String tempDate = readBasicTag(parser, currentTag);
                        channel.setDate(DateParser.toDate(tempDate, DateParser.MONDE_DATE_FORMAT));
                        break;

                    case ITEM_IMAGE:
                        parser.require(XmlPullParser.START_TAG, null, ITEM_IMAGE);
                        int next = parser.next();
                        while (next != XmlPullParser.END_TAG) {
                            String name = parser.getName();

                            if (parser.getEventType() != XmlPullParser.START_TAG) {
                                next = parser.next();
                                continue;
                            }

                            if (name.equalsIgnoreCase("url")) {
                                channel.setImage(readBasicTag(parser, name));
                                break;
                            } else {
                                skip(parser);
                            }
                            next = parser.next();
                        }
                        break;

                    default:
                        break;
                }
            }
        }
        return channel;
    }


    /***
     * Get a stream from the given url
     * @param url
     * @return the content of the feed as an inputstream
     * @throws RequestException
     * @throws UnsupportedEncodingException
     */
    private InputStream getStream(String url) throws RequestException, UnsupportedEncodingException {
        return new Requester()
                .doSyncGet(url);
    }

    /**
     * Fetch the latest articles in a feed
     * @param url
     * @return a list of articles
     * @throws RequestException
     * @throws XmlPullParserException
     * @throws IOException
     * @throws ParseException
     */
    public List<Article> fetchArticles(String url) throws RequestException, XmlPullParserException, IOException, ParseException {
        InputStream stream = getStream(url);
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(stream, null);
        parser.nextTag();
        return readFeed(parser);
    }

    /**
     * Decode a feed attached to an XmlPullParser.
     * @param parser Incoming XMl
     * @return List of {@link Article} objects.
     * @throws org.xmlpull.v1.XmlPullParserException on error parsing feed.
     * @throws java.io.IOException                   on I/O error.
     */
    private List<Article> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException, ParseException {
        List<Article> articles = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, DOC_ROOT);
        int eventType = parser.getEventType();
        String currentTag;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            eventType = parser.next();

            if (eventType == XmlPullParser.START_TAG) {
                currentTag = parser.getName();
                if (currentTag.equals(DOC_ITEM)) {
                    Article article = readEntry(parser);
                    if (article != null) {
                        articles.add(article);
                    }
                }
            }
        }
        return articles;
    }

    /**
     * Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them
     * off to their respective "read" methods for processing. Otherwise, skips the tag.
     */
    private Article readEntry(XmlPullParser parser) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, null, DOC_ITEM);
        String description = null;
        String title = null;
        String link = null;
        String image = null;
        Date publishedOn = null;
        String guid = null;

        while (parser.next() != XmlPullParser.END_TAG) {

            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case ITEM_LINK:
                    link = readBasicTag(parser, name);
                    break;

                case ITEM_TITLE:
                    title = readBasicTag(parser, name);
                    break;

                case ITEM_DESCRIPTION:
                    description = readBasicTag(parser, name);
                    break;

                case ITEM_DATE:
                    String tempDate = readBasicTag(parser, name);
                    publishedOn = DateParser.toDate(tempDate, DateParser.MONDE_DATE_FORMAT);
                    break;

                case ITEM_ENCLOSURE:
                    image = readAlternateLink(parser);
                    parser.next();
                    break;

                case ITEM_GUID:
                    guid = readBasicTag(parser, name);
                    break;

                default:
                    skip(parser);
                    break;
            }
        }
        // make sure that all the required fields are found in the feed
        if (title != null && description != null && link != null && guid != null && publishedOn != null) {
            Article article = new Article(guid, title, description, link, publishedOn);
            if (image != null) {
                article.setImage(image);
            }
            return article;
        } else {
            return null;
        }
    }

    /**
     * Reads the body of a basic XML tag, which is guaranteed not to contain any nested elements.*
     * @param parser Current parser object
     * @param tag    XML element tag name to parse
     * @return Body of the specified tag
     * @throws java.io.IOException
     * @throws org.xmlpull.v1.XmlPullParserException
     */
    private String readBasicTag(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tag);
        String result = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, tag);
        return result;
    }

    /**
     * Processes link tags in the feed.
     */
    private String readAlternateLink(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, ITEM_ENCLOSURE);
        return parser.getAttributeValue(null, "url");
    }

    /**
     * For the tags enclosure, extracts the text value.
     */
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    /**
     * Skips tags the parser isn't interested in. Uses depth to handle nested tags. i.e.,
     * if the next tag after a START_TAG isn't a matching END_TAG, it keeps going until it
     * finds the matching END_TAG (as indicated by the value of "depth" being 0).
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            int i = parser.next();
            if (i == XmlPullParser.END_TAG) {
                depth--;

            } else if (i == XmlPullParser.START_TAG) {
                depth++;

            }
        }
    }
}