package eamosse.gmail.com.instantrss.requester;

/**
 * Created by eamosse on 22/01/2018.
 */

public class RequestException extends Exception {
    private final int code;

    public RequestException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }


}
