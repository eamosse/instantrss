package eamosse.gmail.com.instantrss.requester;

/**
 * Created by edouard on 18/01/2018.
 */

import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * A requester to perform http request
 * Created by edouard on 17/01/2018.
 */

public class Requester {

    /**
     * Execute the request in the same thread as the method which calls the method
     * @param request
     * @return an input stream
     * @throws RequestException
     */
    InputStream executeSync(Request request) throws RequestException {
        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                final ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    return responseBody.byteStream();
                }
            }
            throw new RequestException(response.code(), response.message());
        } catch (IOException e) {
            throw new RequestException(-1, e.getMessage());
        }
    }

    private Request buildQuery(String url) {
        return new Request.Builder().url(url)
                .get()
                .build();
    }

    /**
     * perform a get request asynchronously
     * @param url
     * @return
     * @throws RequestException
     */
    public InputStream doSyncGet(@NonNull String url) throws RequestException {
        Request request = buildQuery(url);
        return executeSync(request);
    }
}
