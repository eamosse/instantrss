package eamosse.gmail.com.instantrss.views;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;

import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.dal.DataStore;
import eamosse.gmail.com.instantrss.helpers.DateParser;
import eamosse.gmail.com.instantrss.helpers.ImageDownloader;
import eamosse.gmail.com.instantrss.models.Article;
import eamosse.gmail.com.instantrss.models.Channel;

/**
 * Manage the article cards
 * Created by eamosse on 17/01/2018.
 */

public class ArticleViewHolder extends RecyclerView.ViewHolder {

    private final CardView cv;
    private final TextView titleTextView;
    private final TextView descriptionTextView;
    private final TextView publicationDateTextView;
    private final TextView articleOptions;
    private final ImageView articleImagePreview;
    private final ImageView publisherIcone;

    public ArticleViewHolder(View itemView) {
        super(itemView);
        cv = itemView.findViewById(R.id.news_card);
        titleTextView = itemView.findViewById(R.id.news_title);
        descriptionTextView = itemView.findViewById(R.id.news_description);
        articleImagePreview = itemView.findViewById(R.id.news_image);
        publisherIcone = itemView.findViewById(R.id.publisher_icon);
        publicationDateTextView = itemView.findViewById(R.id.publish_date);
        articleOptions = itemView.findViewById(R.id.article_options);
    }

    public void bind(final Article article) {
        titleTextView.setText(article.getTitle());
        descriptionTextView.setText(article.getDescription());

        //load the preview image of the article
        ImageDownloader.setImage(article.getImage(), articleImagePreview, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Channel channel = DataStore.getAppDatabase(publisherIcone.getContext()).articleDAO().getChannel(article.getFeedId());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        //load the publisher icon
                        ImageDownloader.setImage(channel.getImage(), publisherIcone, false);
                    }
                });

            }
        }).start();


        //convert the date into duration (regarding the current hour)
        Map<String, Integer> duration = DateParser.duration(article.getPublicationDate());

        //convert the duration into a human readable string (e.g. il y 2 jours)
        String since;
        int value;
        if (duration.get(DateParser.DAYS_KEY) > 0) {
            value = duration.get(DateParser.DAYS_KEY);
            since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_days,
                                                                                          value, value);
        } else if (duration.get(DateParser.HOURS_KEY) > 0) {
            value = duration.get(DateParser.HOURS_KEY);
            since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_hours,
                                                                                          value, value);
        }
        else if (duration.get(DateParser.MINUTES_KEY) > 0) {
            value = duration.get(DateParser.MINUTES_KEY);
            since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_minutes,
                                                                                          value, value);
        } else {
            value = duration.get(DateParser.SECONDS_KEY);
            since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_seconds,
                                                                                         value, value);
        }

        publicationDateTextView.setText(since);

    }

    public CardView getCv() {
        return cv;
    }

    public TextView getArticleOptions() {
        return articleOptions;
    }
}
