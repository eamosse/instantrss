package eamosse.gmail.com.instantrss.views;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import eamosse.gmail.com.instantrss.R;
import eamosse.gmail.com.instantrss.helpers.DateParser;
import eamosse.gmail.com.instantrss.helpers.ImageDownloader;
import eamosse.gmail.com.instantrss.models.Channel;
import eamosse.gmail.com.instantrss.parser.FeedParser;

/**
 * Manage the card for the channels
 * Created by eamosse on 23/01/2018.
 */

public class ChannelViewHolder extends RecyclerView.ViewHolder {

    private final CardView cv;
    private final TextView nameTextView;
    private final TextView descriptionTextView;
    private final ImageView imagePreview;
    private final ImageView addButton;
    private final TextView publicationDateTextView;


    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    private Channel channel;

    public ImageView getAddButton() {
        return addButton;
    }


    public ChannelViewHolder(View itemView) {
        super(itemView);
        cv = itemView.findViewById(R.id.channel_card);
        nameTextView = itemView.findViewById(R.id.channel_name);
        descriptionTextView = itemView.findViewById(R.id.channel_description);
        imagePreview = itemView.findViewById(R.id.publisher_icon);
        addButton = itemView.findViewById(R.id.save_channel);
        publicationDateTextView = itemView.findViewById(R.id.publish_date);
    }

    public void bind(final String url) {

        Task.callInBackground(new Callable<Channel>() {
            @Override
            public Channel call() throws Exception {
                return new FeedParser().fetchFeed(url);
            }
        }).continueWith(new Continuation<Channel, Void>() {
            @Override
            public Void then(Task<Channel> task) throws Exception {
                if (task.isFaulted()) {
                    cv.setEnabled(false);
                } else {
                    channel = task.getResult();
                    cv.setVisibility(View.VISIBLE);
                    ImageDownloader.setImage(channel.getImage(), imagePreview, false);
                    nameTextView.setText(channel.getTitle());
                    descriptionTextView.setText(channel.getDescription());
                    //convert the date into duration (regarding the current hour)
                    Map<String, Integer> duration = DateParser.duration(channel.getDate());

                    //convert the duration into a human readable string (e.g. il y 2 jours)
                    String since;
                    int value;
                    if (duration.get(DateParser.DAYS_KEY) > 0) {
                        value = duration.get(DateParser.DAYS_KEY);
                        since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_days,
                                value, value);
                    } else if (duration.get(DateParser.HOURS_KEY) > 0) {
                        value = duration.get(DateParser.HOURS_KEY);
                        since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_hours,
                                value, value);
                    }
                    else if (duration.get(DateParser.MINUTES_KEY) > 0) {
                        value = duration.get(DateParser.MINUTES_KEY);
                        since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_minutes,
                                value, value);
                    } else {
                        value = duration.get(DateParser.SECONDS_KEY);
                        since = publicationDateTextView.getContext().getResources().getQuantityString(R.plurals.since_seconds,
                                value, value);
                    }
                    since = publicationDateTextView.getContext().getString(R.string.channel_update_since, since);
                    publicationDateTextView.setText(since);
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);

    }

    public CardView getCv() {
        return cv;
    }
}

